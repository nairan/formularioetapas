<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Step 3</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

    <!-- CSS Files -->

    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/css/material-bootstrap-wizard.css')}}" rel="stylesheet" />

    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="{{asset('assets/css/demo.css')}}" rel="stylesheet" />
</head>

<body>

<div class="image-container set-full-height" style="background-image: url('{{asset('assets/img/wizard-book.jpg')}}')">
    <!--   Big container   -->
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <!--      Wizard container        -->
                <div class="wizard-container">
                    <div class="card wizard-card" data-color="red" id="wizard">
                        <form action="{{route('register.step3', $user??'')}}" method="POST">
                            @include('notifications')
                            @method('POST')
                            @csrf
                            <div class="wizard-header">
                                <h3 class="wizard-title">
                                    Cadastro de Usuário
                                </h3>
                                <h5>Preencha as informações das 3 etapa.</h5>

                            </div>
                            <div class="wizard-navigation">
                                <ul>
                                    <li><a href="{{route('register.step1', $user??'')}}" data-toggle="tab">Passo 1</a></li>
                                    <li><a href="{{route('register.step2', $user??'')}}" data-toggle="tab">Passo 2</a></li>
                                    <li class="active"><a href="{{route('register.step3', $user??'')}}" data-toggle="tab">Passo 3</a></li>
                                </ul>
                            </div>

                            <div class="text-center tab-content">
                                <div class="tab-pane active">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <!--<h4 class="info-text"> Informações de Endereço</h4>-->
                                            <h3><b>{{isset($user)? $user->name: ''}}</b></h3>
                                        </div>
                                        <div class="col-sm-6">
                                            <!-- CELULAR -->
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">phonelink_ring</i>
                                                </span>
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Celular</label>
                                                    <input
                                                        name="mobile"
                                                        id="mobile"
                                                        type="text"
                                                        class="form-control"
                                                        required
                                                        placeholder="(DDD) 99999-9999"
                                                        value="{{
                                                            isset($user->contacts)
                                                                ? $user->contacts->mobile
                                                                : @old('mobile')
                                                                ?? ''
                                                        }}"
                                                    >
                                                </div>
                                            </div>
                                            <!-- TELEFONE -->
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">phone</i>
                                                </span>
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Telefone</label>
                                                    <input
                                                        name="phone"
                                                        id="phone"
                                                        type="text"
                                                        class="form-control"
                                                        placeholder="(DDD) 9999-9999"
                                                        required
                                                        value="{{
                                                            isset($user->contacts)
                                                                ? $user->contacts->phone
                                                                : @old('phone')
                                                                ?? ''
                                                        }}"
                                                    >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wizard-footer">
                                <div class="pull-right">
                                    <input
                                        type='submit'
                                        class='btn btn-finish btn-fill btn-danger btn-wd'
                                        name='finish'
                                        value='Finish'
                                    />
                                </div>
                                <div class="pull-left">
                                    <a href="{{route('register.step2', $user??'')}}">
                                        <input
                                            type='button'
                                            class='btn btn-previous btn-fill btn-default btn-wd'
                                            name='previous'
                                            value='Previous'
                                        />
                                    </a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </form>
                    </div>
                </div> <!-- wizard container -->
            </div>
        </div> <!-- row -->
    </div> <!--  big container -->
</div>

</body>
<!--   Core JS Files   -->
<script src="{{asset('assets/js/jquery-2.2.4.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/jquery.bootstrap.js')}}" type="text/javascript"></script>

<!-- mascáras -->
<script src="{{asset('assets/vendors/jQuery-Mask-Plugin-master/dist/jquery.mask.js')}}" type="text/javascript"></script>

<!--  Plugin for the Wizard -->
<script src="{{asset('assets/js/material-bootstrap-wizard.js')}}"></script>

<!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
<script src="{{asset('assets/js/jquery.validate.min.js')}}"></script>

<script>
    $(document).ready(function() {
        <!-- javascript for init -->
        $('#mobile').mask('(999) 99999-9999');
        $('#phone').mask('(999) 9999-9999');
    });
</script>
</html>
