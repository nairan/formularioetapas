<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Step 2</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

    <!-- CSS Files -->

    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/css/material-bootstrap-wizard.css')}}" rel="stylesheet" />

    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="{{asset('assets/css/demo.css')}}" rel="stylesheet" />
</head>

<body>
<div class="image-container set-full-height" style="background-image: url('{{asset('assets/img/wizard-book.jpg')}}')">

    <!--   Big container   -->
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <!--      Wizard container        -->
                <div class="wizard-container">
                    <div class="card wizard-card" data-color="red" id="wizard">
                        <form action="{{route('register.step2', $user)}}" method="POST">
                            @include('notifications')
                            @method('POST')
                            @csrf
                            <div class="wizard-header">
                                <h3 class="wizard-title">
                                    Cadastro de Usuário
                                </h3>
                                <h5>Preencha as informações das 3 etapa.</h5>
                            </div>
                            <div class="wizard-navigation">
                                <ul>
                                    <li><a href="{{route('register.step1', $user??'')}}" data-toggle="tab">Passo 1</a></li>
                                    <li class="active"><a href="{{route('register.step2', $user??'')}}" data-toggle="tab">Passo 2</a></li>
                                    <li><a href="{{route('register.step3', $user??'')}}" data-toggle="tab">Passo 3</a></li>
                                </ul>
                            </div>

                            <div class="text-center tab-content">
                                <div class="tab-pane active">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <!--<h4 class="info-text"> Informações de Endereço</h4>-->
                                            <h3><b>{{isset($user)? $user->name: ''}}</b></h3>
                                        </div>
                                        <div class="col-sm-6">
                                            <!-- ENDEREÇO/RUA -->
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">house</i>
                                                </span>
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Rua</label>
                                                    <input
                                                        name="street"
                                                        id="street"
                                                        type="text"
                                                        class="form-control"
                                                        required
                                                        placeholder="Insira seu endereço"
                                                        value="{{
                                                            isset($user->address)
                                                                ? $user->address->street
                                                                : @old('street')
                                                                ?? ''
                                                        }}"
                                                    >
                                                </div>
                                            </div>
                                            <!-- NÚMERO ENDEREÇO -->
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">format_list_numbered_rtl</i>
                                                </span>
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Número</label>
                                                    <input
                                                        name="number"
                                                        id="number"
                                                        type="text"
                                                        class="form-control"
                                                        required
                                                        placeholder="Insira o Número do endereço"
                                                        value="{{
                                                            isset($user->address)
                                                                ? $user->address->number
                                                                : @old('number')
                                                                ?? ''
                                                        }}"
                                                    >
                                                </div>
                                            </div>
                                            <!-- CEP -->
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">confirmation_number</i>
                                                </span>
                                                <div class="form-group label-floating">
                                                    <label class="control-label">CEP</label>
                                                    <input
                                                        name="cep"
                                                        id="cep"
                                                        type="text"
                                                        class="form-control"
                                                        required
                                                        placeholder="99999-999"
                                                        value="{{
                                                            isset($user->address)
                                                                ? $user->address->cep
                                                                : @old('cep')
                                                                ?? ''
                                                        }}"
                                                    >
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Estado</label>
                                                <select class="form-control" name="state" id="state" required>
                                                    <option value="">Selecione um Estado</option>
                                                    @foreach($states as $state)
                                                        <option
                                                            value="{{$state->id}}"
                                                            {{
                                                                isset($user->address)
                                                                && $user->address->state_id == $state->id
                                                                    ? 'selected'
                                                                    : ''
                                                            }}
                                                        > {{$state->name}} </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Cidade</label>
                                                <select class="form-control" id="city" name="city" required {{empty($cities)?'disabled':''}}>

                                                    @forelse($cities as $city)
                                                        <option
                                                            value="{{$city->id}}"
                                                            {{
                                                                isset($user->address)
                                                                && $user->address->city_id == $city->id
                                                                    ? 'selected'
                                                                    : ''
                                                            }}
                                                        > {{$city->name}} </option>
                                                    @empty
                                                        <option value="">Selecione uma Estado</option>
                                                    @endforelse
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wizard-footer">
                                <div class="pull-right">
                                    <a href="{{route('register.step3', $user)}}">
                                        <input
                                            type='submit'
                                            class='btn btn-next btn-fill btn-danger btn-wd'
                                            name='proximo'
                                            value='Próximo'
                                        />
                                    </a>
                                </div>
                                <div class="pull-left">
                                    <a href="{{route('register.step1', $user->id)}}">
                                        <input
                                            type='button'
                                            class='btn btn-previous btn-fill btn-default btn-wd'
                                            name='anterior'
                                            value='Anterior'
                                        />
                                    </a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </form>
                    </div>
                </div> <!-- wizard container -->
            </div>
        </div> <!-- row -->
    </div> <!--  big container -->
</div>

</body>
<!--   Core JS Files   -->
<script src="{{asset('assets/js/jquery-2.2.4.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/jquery.bootstrap.js')}}" type="text/javascript"></script>

<!-- mascáras -->
<script src="{{asset('assets/vendors/jQuery-Mask-Plugin-master/dist/jquery.mask.js')}}" type="text/javascript"></script>

<!--  Plugin for the Wizard -->
<script src="{{asset('assets/js/material-bootstrap-wizard.js')}}"></script>

<!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
<script src="{{asset('assets/js/jquery.validate.min.js')}}"></script>
<script>

    /**  javascript for init */
    let $state = $("#state");
    let $city  = $("#city");

    $(document).ready(function() {
        $('#cep').mask('99999-999');
        $('#number').mask('99999');

        $state.change(function() {
            $.get("{{route('cities')}}" + "/"+ this.value, function(response) {
                $city.attr('disabled', false).children().remove();
                $city.append(new Option('Selecione uma Cidade', ""));
                $.each(response, function() {
                    $city.append(new Option(this.name, this.id));
                });
            });
        });
    });
</script>
</html>
