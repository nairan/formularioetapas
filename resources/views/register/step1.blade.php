<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Step 1</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

    <!-- CSS Files -->
    <link rel="stylesheet" href="{{asset('assets/css/jquery-datepicker.css')}}">
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/css/material-bootstrap-wizard.css')}}" rel="stylesheet" />

    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="{{asset('assets/css/demo.css')}}" rel="stylesheet" />
</head>

<body>

<div class="image-container set-full-height" style="background-image: url('{{asset('assets/img/wizard-book.jpg')}}')">

    <!--   Big container   -->
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <!--      Wizard container        -->
                <div class="wizard-container">
                    <div class="card wizard-card" data-color="red" id="wizard">
                        <form action="{{route('register.step1', $user)}}" method="POST">
                            @include('notifications')
                            @method('POST')
                            @csrf
                            <div class="wizard-header">
                                <h3 class="wizard-title">
                                    Cadastro de Usuário
                                </h3>
                                <h5>Preencha as informações das 3 etapa.</h5>
                            </div>
                            <div class="wizard-navigation">
                                <ul>
                                    <li class="active"><a href="#" data-toggle="tab">Passo 1</a></li>
                                    <li><a>Passo 2</a></li>
                                    <li><a>Passo 3</a></li>
                                </ul>
                            </div>

                            <div class="text-center active">
                                <div class="tab-pane">
                                    <h4 class="info-text">Primera Etapa: Dados pessoais </h4>
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">account_circle</i>
													</span>
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Nome</label>
                                                    <input
                                                        name="name"
                                                        type="text"
                                                        class="form-control"
                                                        required
                                                        placeholder="Insira seu nome completo"
                                                        value="{{
                                                            isset($user) && isset($user->name)
                                                                ? $user->name
                                                                :  @old('name')
                                                                ?? ''
                                                        }}"
                                                    >
                                                </div>
                                            </div>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">date_range</i>
                                                </span>
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Data de Nascimento</label>
                                                    <input
                                                        type="text"
                                                        class="form-control datepicker"
                                                        name="birthDate"
                                                        id="birthDate"
                                                        required
                                                        placeholder="99/99/9999"
                                                        autocomplete="off"
                                                        value="{{
                                                            isset($user) && isset($user->birth_date)
                                                                ? $user->birth_date
                                                                : @old('birth_date')
                                                                ?? ''
                                                        }}"
                                                    >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wizard-footer">
                                <div class="pull-right">
                                    <input
                                        type='submit'
                                        class='btn btn-fill btn-danger btn-wd'
                                        name='next'
                                        value='Próximo'
                                    />
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </form>
                    </div>
                </div> <!-- wizard container -->
            </div>
        </div> <!-- row -->
    </div> <!--  big container -->
</div>

</body>
<!--   Core JS Files   -->
<script src="{{asset('assets/js/jquery-2.2.4.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/jquery.bootstrap.js')}}" type="text/javascript"></script>

<!-- mascáras -->
<script src="{{asset('assets/vendors/jQuery-Mask-Plugin-master/dist/jquery.mask.js')}}" type="text/javascript"></script>

<!--  Plugin for the Wizard -->
<script src="{{asset('assets/js/material-bootstrap-wizard.js')}}"></script>

<!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
<script src="{{asset('assets/js/jquery.validate.min.js')}}"></script>
<script src="{{asset('assets/js/jquery-datepicker.js')}}" type="text/javascript"></script>
<script>
    /** javascript for init */
    $('#birthDate').mask('00/00/0000');
    $('.datepicker').datepicker({
        autoclose: true,
        format: 'mm/dd/yyyy',
        regional: 'pt-BR',
        todayHighlight: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        changeMonth: true,
        changeYear: true,
        minDate: '-100Y',
        maxDate: '+0D',
    });
</script>
</html>
