@if ($errors->any())
<div class="alert alert-danger alert-dismissable mt-2">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <strong>Erro!</strong> {!! implode('', $errors->all('<div>:message</div>')) !!}
</div>
@endif

@if (Session::has('success'))
<div class="alert alert-success alert-dismissable mt-2">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Sucesso!</strong> {{ Session::get('success') }}
</div>
@endif

@if (Session::has('error'))
<div class="alert alert-danger alert-dismissable mt-2">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Erro!</strong> {{ Session::get('error') }}
</div>

@endif

@if (Session::has('warning'))
<div class="alert alert-warning alert-dismissable mt-2">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Alerta!</strong> {{ Session::get('warning') }}
</div>
@endif

@if (Session::has('info'))
<div class="alert alert-info alert-dismissable mt-2">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Info!</strong> {{ Session::get('info') }}
</div>
@endif
