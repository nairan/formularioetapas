<?php


namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Step1Request extends FormRequest
{
    /**
     * @author Nairan Omura <nairanomura@hotmail.com>
     * @since 12/02/2020
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @author Nairan Omura <nairanomura@hotmail.com>
     * @since 12/02/2020
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'name'      => 'required|max:50|min:5',
                    'birthDate' => 'required|max:10|min:10',
                ];
            default:
                break;
        }
    }
}
