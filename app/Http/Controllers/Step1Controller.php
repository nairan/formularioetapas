<?php

namespace App\Http\Controllers;

use App\Http\Requests\Step1Request;
use App\Model\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\View\View;

class Step1Controller extends Controller
{
    /**
     * Exibe a tela de primeira etapa de cadastro
     *
     * @author Nairan Omura <nairanomura@hotmail.com>
     * @since 11/02/2020
     *
     * @param User $user
     * @return Factory|View
     */
    public function index(User $user)
    {
        return view('register.step1', compact('user'));
    }

    /**
     * Registra a primeira etapa do formulário com os dados do usuário
     *
     * @author Nairan Omura <nairanomura@hotmail.com>
     * @since 11/02/2020
     *
     * @param Step1Request $request
     * @param User $user
     * @return Factory|RedirectResponse|View
     * @throws Exception
     */
    public function store(Step1Request $request, User $user)
    {
        try {
            $date = Carbon::createFromFormat('d/m/Y', $request->input('birthDate'));
            $attributes = [
                'name'       => $request->input('name'),
                'birth_date' => $date->format('Y-m-d')
            ];

            if (!empty($user = User::updateOrCreate(['id' => $user->id], $attributes))) {
                return redirect()->route('register.step2', $user);
            }
            return back()->with('error', 'Falha ao cadastrar usuário!');
        } catch (Exception $e) {
            throw $e;
        }
    }
}
