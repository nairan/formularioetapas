<?php


namespace App\Http\Controllers;


use App\Http\Requests\Step2Request;
use App\Model\City;
use App\Model\State;
use App\Model\User;
use App\Model\UserAddress;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\View\View;

class Step2Controller extends Controller
{
    /**
     * Exibe a segunda etapa do formulário de cadastro
     *
     * @author Nairan Omura <nairanomura@hotmail.com>
     * @since 10/02/2020
     *
     * @param User $user
     * @return Factory|RedirectResponse|View
     */
    public function index(User $user)
    {
        if (!empty($user->id)) {
            $states = State::get(['id', 'name']);
            $cities = $user->address
                        ? City::where('state_id', $user->address->state_id)->get(['id', 'name'])
                        : [];

            return view('register.step2', compact(
                'user',
                'states',
                'cities'
            ));
        }
        return redirect()->route('register.step1');
    }

    /**
     * Grava dados de endereço do usuário para segunda etapa do formulário
     *
     * @author Nairan Omura <nairanomura@hotmail.com>
     * @since 10/02/2020
     *
     * @param Step2Request $request
     * @param User $user
     * @return Factory|RedirectResponse|View
     * @throws Exception
     */
    public function store(Step2Request $request, User $user)
    {
        try {
            $result = UserAddress::updateOrcreate([
                'user_id' => $user->id
            ], [
                'user_id'  => $user->id,
                'street'   => $request->input('street'),
                'cep'      => $request->input('cep'),
                'number'   => $request->input('number'),
                'city_id'  => $request->input('city'),
                'state_id' => $request->input('state')
            ]);

            if ($result) {
                return redirect()->route('register.step3', $user);
            }
            return back()->with('error', 'Falha ao cadastrar dados de endereço');
        } catch (Exception $e) {
            throw $e;
        }
    }
}
