<?php

namespace App\Http\Controllers;

use App\Http\Requests\Step3Request;
use App\Model\User;
use App\Model\UserContact;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\View\View;

class Step3Controller extends Controller
{
    /**
     * Exibe o formulário da terceira etapa do cadastro
     *
     * @author Nairan Omura <nairanomura@hotmail.com>
     * @since 10/02/2020
     *
     * @param User $user
     * @return Factory|View
     */
    public function index(User $user)
    {
        if (!empty($user->id)) {
            return view('register.step3', compact('user'));
        }
        return redirect()->route('register.step1', $user);
    }

    /**
     * Registra os dados da terceira etapa do formulário
     *
     * @author Nairan Omura <nairanomura@hotmail.com>
     * @since 10/02/2020
     *
     * @param Step3Request $request
     * @param User $user
     * @return RedirectResponse
     * @throws Exception
     */
    public function store(Step3Request $request, User $user)
    {
        try {
            $result = UserContact::updateOrcreate([
                'user_id' => $user->id
            ], [
                'user_id' => $user->id,
                'mobile' => $request->input('mobile'),
                'phone' => $request->input('phone')
            ]);

            if ($result) {
                return redirect()->route('register.step1')->with(
                    'success',
                    'Usuário Registrado com sucesso!'
                );
            }
            return back()->with('error', 'Falha ao cadastrar dados de endereço');
        } catch (Exception $e) {
            throw $e;
        }
    }
}
