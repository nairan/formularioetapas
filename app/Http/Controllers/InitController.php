<?php

namespace App\Http\Controllers;

use App\Model\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Controller;

class InitController extends Controller
{
    /**
     * Classe Inicial, verifica se há um cadastro em andamento,
     * caso haja, redireciona para a última etapa preenchida
     *
     * @author Nairan Omura <nairanomura@hotmail.com>
     * @since 12/02/2020
     *
     * @return RedirectResponse
     */
    public function index()
    {
        if ($user = User::has('address')->doesntHave('contacts')->first()) {
            return redirect()->route('register.step3', $user);
        }

        if ($user = User::doesntHave('contacts')->doesntHave('address')->first()) {
            return redirect()->route('register.step2', $user);
        }

        return redirect()->route('register.step1');
    }
}
