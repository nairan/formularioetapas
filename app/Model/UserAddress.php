<?php


namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    protected $table = 'user_address';
    protected $fillable = [
        'id',
        'user_id',
        'street',
        'number',
        'cep',
        'city_id',
        'state_id',
        'created_at',
        'updated_at'
    ];
}
