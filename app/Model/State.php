<?php


namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table = 'states';
    protected $fillable = [
        'id',
        'name',
        'abbr',
        'updated_at',
        'created_at'
    ];
}
