<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';
    protected $fillable = [
        'id',
        'name',
        'birth_date',
        'created_at',
        'updated_at'
    ];

    /**
     * @author Nairan Omura <nairanomura@hotmail.com>
     * @since 11/02/2020
     *
     * @param $value
     * @return string
     */
    public function getBirthDateAttribute($value)
    {
        return $value
                ? Carbon::createFromFormat('Y-m-d', $value)->format('d/m/Y')
                : null;
    }

    public function address()
    {
        return $this->hasOne(UserAddress::class);
    }

    public function contacts()
    {
        return $this->hasOne(UserContact::class);
    }
}
