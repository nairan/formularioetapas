<?php


namespace App\Model;


use Illuminate\Database\Eloquent\Model;

class UserContact extends Model
{
    protected $table = 'user_contacts';
    protected $fillable = [
        'id',
        'user_id',
        'mobile',
        'phone',
        'created_at',
        'updated_at'
    ];
}
