<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'InitController@index');

Route::middleware([])->name('register.')->prefix('register')->group(function () {
    /**
     * Step 1
     */
    Route::name('step1')->prefix('step1')->get('/{user?}', 'Step1Controller@index');
    Route::name('step1')->prefix('step1')->post('/{user?}', 'Step1Controller@store');

    /**
     * Step 2
     */
    Route::name('step2')->prefix('step2')->get('/{user}', 'Step2Controller@index');
    Route::name('step2')->prefix('step2')->post('/{user}', 'Step2Controller@store');

    /**
     * Step 3
     */
    Route::name('step3')->prefix('step3')->get('/{user}', 'Step3Controller@index');
    Route::name('step3')->prefix('step3')->post('/{user}', 'Step3Controller@store');
});

