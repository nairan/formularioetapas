<?php

use App\Model\City;
use App\Model\State;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware([])->name('cities')->get('/cities/{stateId?}', function($stateId) {
    return City::where('state_id', $stateId)->get(['id', 'state_id', 'name']);
});

Route::middleware([])->name('states')->get('/states', function() {
    return State::get(['id', 'name', 'abbr']);
});
