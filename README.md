# formularioEtapas
Formulário de cadastro de usuário em 3 etapas desenvolvido com LARAVEL.

<img src="https://gitlab.com/nairan/formularioetapas/-/raw/master/public/assets/img/laravel-three_steps_form.png" width="70%" /><br>


# Execução do Projeto
1. Realizar a instalação do  [Laragon](https://sourceforge.net/projects/laragon/files/releases/4.0/laragon-full.exe/download). (passo-a-passo de instalação no final do arquivo)
2. Abra o <b>terminal do laragon</b> ou <b>terminal git</b> (estilo bash): <br>
    <img src="https://gitlab.com/nairan/formularioetapas/-/raw/master/public/assets/img/commander.png" width="27%" />
3. Navegue até a pasta <b>C:\laragon\www</b>:<br>
   `cd c:\laragon\www`
4. Realize o clone do projeto <br>
    `git clone https://gitlab.com/nairan/formularioetapas.git`
5. Acesse a pasta do projeto <br>
    `cd formularioetapas`
6. Instale os pacotes dependentes via composer <br>
    `composer install`
7. Crie o arquivo .env do projeto <br>
    `cp .env.example .env`
9. Crie o schema no mysql através do comando <br>
    `php artisan mysql:createdb laravel`
10. Execute as migrations <br>
    `php artisan migrate`
11. Execute as seeders <br>
    `php artisan db:seed --class=StatesTableSeeder`
    <br>
    `php artisan db:seed --class=CitiesTableSeeder`
12. Gere a chave de criptografia do projeto <br>
    `php artisan key:generate`
13. Inicie o laragon <br>
    <img src="https://gitlab.com/nairan/formularioetapas/-/raw/master/public/assets/img/start.png" width="27%" />
14. Acesse a URL => <b>formularioetapas.test<b>


# Instalação Laragon => WINDOWS
Para criar e executar o projeto foi utilizado a stack [Laragon](https://sourceforge.net/projects/laragon/files/releases/4.0/laragon-full.exe/download)
para facilitar a instalação e execução das ferramentas necessárias para o desenvolvimento do projeto.
Portanto, para rodar o projeto recomenda-se utilizar o [Laragon](https://sourceforge.net/projects/laragon/files/releases/4.0/laragon-full.exe/download).

<h5>1. Após a instalação, Clique no ícone de configuração:</h5>
<img src="https://gitlab.com/nairan/formularioetapas/-/raw/master/public/assets/img/step1.png" width="37%" />

<h5>2. Acesse a aba "Serviços e Portas" e ative os MySQL + NGINX + REDIS</h5>
<img src="https://gitlab.com/nairan/formularioetapas/-/raw/master/public/assets/img/config.png" width="37%" />

<h5>3. Inicie os serviços do Laragon</h5>
<img src="https://gitlab.com/nairan/formularioetapas/-/raw/master/public/assets/img/start.png" width="37%" />

# Template
Para criação do front-end foi utilizado um template em bootstrap do site [creative-tim](https://www.creative-tim.com/) e 
adaptado para o padrão de views .blade do laravel e adicionadas algumas bibliotecas JQuery externas:

<h4>Template</h4>
    * [https://www.creative-tim.com/product/material-bootstrap-wizard](https://www.creative-tim.com/product/material-bootstrap-wizard)
<h4>Bibliotecas JQUERY</h4>
    * [jquery.validator](https://jqueryvalidation.org/)<br>
    * [jquery.mask](https://igorescobar.github.io/jQuery-Mask-Plugin/)<br>
    * [jquery.ui] (https://jqueryui.com/datepicker/)


